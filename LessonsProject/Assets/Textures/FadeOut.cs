﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : MonoBehaviour
{
    [SerializeField]
    private AnimationCurve curve;

    private float time = 2f;
    private static readonly int ColorId = Shader.PropertyToID("_Color");

    IEnumerator  Start()
    {
        var renderer = GetComponent<MeshRenderer>();
        var material = renderer.material;
        var color = material.GetColor(ColorId);
        
        float k = 0f;

        while (k < time)
        {
            var a = k / time;
            color.a = 1 - a;
            k += Time.deltaTime;
            
            material.SetColor(ColorId, color);
            
            yield return null;
        }
    }

}
