﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformsGenerator : MonoBehaviour
{
    [SerializeField] private GameObject platformPrefab;
    [SerializeField] private Transform target;
    [SerializeField] private float distance;
    [SerializeField] private float min = 10f;
    [SerializeField] private float max = 10f;

    private List<Transform> platforms = new List<Transform>();
    
    void Start()
    {
        SetupPlatforms();
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlatforms();
    }

    private void SetupPlatforms()
    {
        var minY = target.position.y - min;
        var maxY = target.position.y + max;
        
        var pos = new Vector3(0f, minY, 0f);
        while (pos.y < maxY)
        {
            AddPlatform(pos);
            pos.y += distance;
        }
    }

    private void UpdatePlatforms()
    {
        var minY = target.position.y - min;
        var maxY = target.position.y + max;

        foreach (var platform in platforms)
        {
            if (platform.position.y < minY)
            {
                platforms.Remove(platform);
                platform.gameObject.SetActive(false);
                break;
            }
        }

        var pos = platforms[platforms.Count - 1].position;
        pos.y += distance;
        
        while (pos.y < maxY)
        {
            AddPlatform(pos);
            pos.y += distance;
        }
        
    }

    private void AddPlatform(Vector3 pos)
    {
        var obj = ObjectsPool.Instance.GetObject(platformPrefab);
        obj.transform.position = pos;
            
        platforms.Add(obj.transform);
    }
}
