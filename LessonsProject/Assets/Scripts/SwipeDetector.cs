﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    public event Action<int> OnSwipe;

    private float startX;
    
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startX = Input.mousePosition.x;
        }

        if (Input.GetMouseButtonUp(0))
        {
            var x = Input.mousePosition.x;

            if (Mathf.Abs(x - startX) > 100)
            {
                OnSwipe?.Invoke(x > startX ? 1 : -1);
            }
        }
    }
}
