﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMeshAnimation : MonoBehaviour
{
    private MeshFilter meshFilter;
    private const int Length = 9;

    private float bend;

    [SerializeField] private AnimationCurve curve;

    private void Start()
    {
        meshFilter = gameObject.GetComponent<MeshFilter>();
        StartCoroutine(BendProcess());
    }

    private void Update()
    {
        GenerateLine();
    }

    private IEnumerator BendProcess()
    {
        while (bend < 1f)
        {
            bend += Time.deltaTime;
            yield return null;
        }
        
        while (bend > 0f)
        {
            bend -= Time.deltaTime;
            yield return null;
        }

        StartCoroutine(BendProcess());
    }

    private void GenerateLine()
    {
        var vertices = new List<Vector3>();
        var triangles = new List<int>();

        var currentVertex = 0;
        for (int x = 0; x < Length; x++)
        {
            for (int y = 0; y < 2; y++)
            {
                var k = (float) x / Length;
                var pZ = curve.Evaluate(k) * 3f;
                pZ *= bend;
                
                vertices.Add(new Vector3(x, y, pZ));

                if (x == 0 || x == Length - 1)
                {
                    currentVertex++;
                    continue;
                }

                if (y == 0)
                {
                    triangles.Add(currentVertex - 2);
                    triangles.Add(currentVertex - 1);
                    triangles.Add(currentVertex);
                }

                if (y == 1)
                {
                    triangles.Add(currentVertex - 2);
                    triangles.Add(currentVertex);
                    triangles.Add(currentVertex - 1);
                }
                currentVertex++;
            }
        }

        var mesh = new Mesh
        {
            vertices = vertices.ToArray(),
            triangles = triangles.ToArray()
        };
        
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        meshFilter.sharedMesh = mesh;


    }
}
