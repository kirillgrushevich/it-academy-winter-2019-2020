﻿using Unity.Entities;

namespace ECS
{
    [GenerateAuthoringComponent]
    public struct EffectLifeComponent : IComponentData
    {
        public float LifeTime;
    }
}
