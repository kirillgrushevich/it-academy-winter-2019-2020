﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TestUiClick : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {
        

        if (Input.GetMouseButtonDown(0))
        {
            var isUi = EventSystem.current.currentSelectedGameObject != null;
            
            Debug.Log(isUi ? "UI" : "Click");
        }
    }
}
