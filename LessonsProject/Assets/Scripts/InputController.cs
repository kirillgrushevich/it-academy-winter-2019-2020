﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum InputCommand
{
    Fire,
    Skill,
    DeadShot,
}

public class InputController : MonoBehaviour
{
    public static Action<InputCommand> OnInputAction;

    [SerializeField] private Button fireButton;
    [SerializeField] private Button skillButton;
    [SerializeField] private Button deadShotButton;
    
    // Start is called before the first frame update
    private void Awake()
    {
        fireButton.onClick.AddListener(OnFireButton);
        skillButton.onClick.AddListener(OnSkillButton);
        deadShotButton.onClick.AddListener(OnDeadShotButton);
    }

    private void OnFireButton()
    {
        OnInputAction?.Invoke(InputCommand.Fire);
    }
    
    private void OnSkillButton()
    {
        OnInputAction?.Invoke(InputCommand.Skill);
    }

    private void OnDeadShotButton()
    {
        OnInputAction?.Invoke(InputCommand.DeadShot);
    }
}
