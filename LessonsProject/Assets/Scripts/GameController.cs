using UnityEngine;


public class GameController : SingletonGameObject<GameController>
{
    public int Gold
    {
        get => Profile.Money;
        set
        {
            Profile.Money = value;
            Profile.Save(player:false);
            Debug.Log(Profile.Money);
        }
    }
}
