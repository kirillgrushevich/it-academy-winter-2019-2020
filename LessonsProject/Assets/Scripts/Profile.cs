﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Profile
{
    [System.Serializable]
    public class MainData
    {
        public List<int> levelStars = new List<int>();
        public int money = 10;
    }
    
    [System.Serializable]
    public class PlayerData
    {
        public bool sound = true;
        public bool music = true;
    }

    private static MainData mainData;
    private static PlayerData playerData;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void CheckData()
    {
        CheckMainData();
        CheckPlayerData();
    }

    private static void CheckMainData()
    {
        Debug.Log("Init Profile Main Data");
        if (mainData != null)
        {
            return;
        }

        mainData = GetData<MainData>("MainData");
    }

    private static void CheckPlayerData()
    {
        Debug.Log("Init Profile Player Data");
        if (playerData != null)
        {
            return;
        }

        playerData = GetData<PlayerData>("PlayerData");
    }

    private static T GetData<T>(string key) where T : new ()
    {
        if (PlayerPrefs.HasKey(key))
        {
            return JsonUtility.FromJson<T>(PlayerPrefs.GetString(key));
        }
        
        var data = new T();
        PlayerPrefs.SetString(key, JsonUtility.ToJson(data));
        return data;
    }

    public static void Save(bool main = true, bool player = true)
    {
        if (main)
        {
            PlayerPrefs.SetString("MainData", JsonUtility.ToJson(mainData));
        }

        if (player)
        {
            PlayerPrefs.SetString("PlayerData", JsonUtility.ToJson(playerData));
        }
    }

    public static int Money
    {
        get => mainData.money;
        set
        {
            mainData.money = value;
            if (mainData.money < 0)
            {
                mainData.money = 0;
            }
            Save(player:false);
        }
    }

    public static int OpenedLevelsCount => mainData.levelStars.Count;

    public static int GetLevelStars(int level)
    {
        if (level >= mainData.levelStars.Count)
        {
            return -1;
        }

        return mainData.levelStars[level];
    }

    public static void SetLevelStars(int level, int stars)
    {
        if (level > mainData.levelStars.Count)
        {
            Debug.LogError($"Level {level} was not open");
            return;
        }

        if (level == mainData.levelStars.Count)
        {
            stars = Mathf.Clamp(stars, 0, 3);
            mainData.levelStars.Add(stars);
            Save(player:false);
            return;
        }

        if (stars > mainData.levelStars[level])
        {
            stars = Mathf.Clamp(stars, 0, 3);
            mainData.levelStars[level] = stars;
            Save(player:false);
        }
    }
}
