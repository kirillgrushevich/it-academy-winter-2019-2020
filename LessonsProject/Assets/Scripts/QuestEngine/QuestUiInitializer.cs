using System;
using QuestEngine.Data;
using UnityEngine;

namespace QuestEngine
{
    public class QuestUiInitializer : MonoBehaviour
    {
        [SerializeField] private TextAsset asset;

        private void Start()
        {
            var data = JsonUtility.FromJson<QuestData>(asset.text);
            QuestEngine.Instance.SetupQuest(data);
        }
    }
}