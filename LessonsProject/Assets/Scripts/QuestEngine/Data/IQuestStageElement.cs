namespace QuestEngine.Data
{
    public interface IQuestStageElement
    {
        string Id { get; }
        void Complete();
    }
}