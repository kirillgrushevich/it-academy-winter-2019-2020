namespace QuestEngine.Data
{
    [System.Serializable]
    public class QuestStageInfo
    {
        public string id;
        public int queue;
        public string[] elements;
    }
}