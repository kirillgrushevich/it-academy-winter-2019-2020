namespace QuestEngine.Data
{
    public interface IQuestStage
    {
        string Id { get; }
        string[] Elements { get; }
        void Start();
        void Check();
        void Complete();
        void Fail();
    }
}