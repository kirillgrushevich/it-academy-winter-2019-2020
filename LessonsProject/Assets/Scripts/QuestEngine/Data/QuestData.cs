namespace QuestEngine.Data
{
    [System.Serializable]
    public class QuestData
    {
        public string nameId;
        public bool inTurn;
        public QuestStageInfo[] stages;
        public QuestStageElementInfo[] elements;
    }
}