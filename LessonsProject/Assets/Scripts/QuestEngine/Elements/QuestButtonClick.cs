using System;
using UnityEngine;
using UnityEngine.UI;

namespace QuestEngine.Elements
{
    [RequireComponent(typeof(Button))]
    public class QuestButtonClick : MonoBehaviour
    {
        [SerializeField] private string elementId;
        [SerializeField] private Button button;

        private void Reset()
        {
            button = GetComponent<Button>();
        }

        private void Start()
        {
            button.onClick.AddListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
             QuestEngine.Instance.CompleteElement(elementId);  
        }
    }
}