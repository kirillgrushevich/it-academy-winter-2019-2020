﻿using System.Linq;
using QuestEngine.Data;
using UnityEngine;

namespace QuestEngine
{
    public class QuestEngine : SingletonObject<QuestEngine>
    {
        private QuestData questData;
        private int currentStage;
        
        public void SetupQuest(QuestData data)
        {
            questData = data;
            currentStage = 0;
        }
        
        public void CompleteElement(string id)
        {
            var stage = questData.stages.FirstOrDefault(
                s => s.queue == currentStage);

            if (stage == null)
            {
                return;
            }

            for (var i = 0; i < stage.elements.Length; i++)
            {
                if (questData.inTurn && !string.IsNullOrEmpty(stage.elements[i]))
                {
                    if (stage.elements[i] == id)
                    {
                        stage.elements[i] = null;
                    }
                    break;
                }
                
                if (stage.elements[i] == id)
                {
                    stage.elements[i] = null;
                    break;
                }
            }

            foreach (var element in stage.elements)
            {
                if (!string.IsNullOrEmpty(element))
                {
                    return;
                }
            }
            
            Debug.Log($"Stage {currentStage} complete");
            currentStage++;
            
            stage = questData.stages.FirstOrDefault(
                s => s.queue == currentStage);

            if (stage == null)
            {
                Debug.Log($"Quest {questData.nameId} has completed");
            }
        }
    }
}
