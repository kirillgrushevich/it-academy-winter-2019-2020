using System.IO;
using QuestEngine.Data;
using UnityEditor;
using UnityEngine;

namespace QuestEngine.Editor
{
    public class QuestEditor : EditorWindow
    {
        public QuestData data;
        
        [MenuItem("Tools/Quest Engine/Quest Editor")]
        private static void ShowWindow()
        {
            var window = GetWindow<QuestEditor>();
            window.titleContent = new GUIContent("Quest Editor");
            window.Show();
        }

        private void OnGUI()
        {
            if (GUILayout.Button("New"))
            {
                data = new QuestData();
            }

            if (GUILayout.Button("Load"))
            {
                Load();
            }

            if (data == null)
            {
                return;
            }

            if (GUILayout.Button("Save"))
            {
                Save();
            }

            var serializedObject = new SerializedObject(this);
            var serializedProperty = serializedObject.FindProperty("data");
            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();
        }

        private void  Load()
        {
            var filePath = EditorUtility.OpenFilePanel("Select data file", 
                Application.dataPath, "json");
            if (string.IsNullOrEmpty(filePath)) return;
            var dataAsJson = File.ReadAllText(filePath);
            data = JsonUtility.FromJson<QuestData>(dataAsJson);
        }

        private void Save()
        {
            var filePath = EditorUtility.SaveFilePanel("Save data file", 
                Application.dataPath, "", "json");
            if (string.IsNullOrEmpty(filePath)) return;
            var dataAsJson = JsonUtility.ToJson(data);
            File.WriteAllText(filePath, dataAsJson);
            AssetDatabase.Refresh();
        }
    }
}