using UnityEngine;


[CreateAssetMenu(fileName = "EffectsConfig", menuName = "EffectsConfig", order = 0)]
public class EffectsConfig : ScriptableObject
{
    public GameObject[] effects;
}
