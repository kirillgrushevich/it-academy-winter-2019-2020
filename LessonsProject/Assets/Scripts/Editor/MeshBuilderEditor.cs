﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
public class MeshBuilderEditor : EditorWindow
{
    private static SerializableMeshInfo meshInfo;
    private const string LevelDataPath = "Assets/Data/LevelsData/";

    [MenuItem("Tools/Mesh/SaveMesh")]
    private static void SaveSelectedMesh()
    {
        var obj = Selection.activeGameObject;
        if (obj == null)
        {
            return;
        }

        var meshFilter = obj.GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            return;
        }

        var meshRenderer = obj.GetComponent<MeshRenderer>();
        if (meshRenderer == null)
        {
            return;
        }
        
        meshInfo = new SerializableMeshInfo(
            obj.name, meshFilter.sharedMesh, meshRenderer.sharedMaterial);

        var json = JsonUtility.ToJson(meshInfo);

        PlayerPrefs.SetString("MeshInfo", json);
    }

    [MenuItem("Tools/Mesh/Load mesh")]
    private static void LoadMesh()
    {
        if (meshInfo == null && PlayerPrefs.HasKey("MeshInfo"))
        {
            Debug.Log("Loaded from Player Prefs");
            var json = PlayerPrefs.GetString("MeshInfo");
            meshInfo = JsonUtility.FromJson<SerializableMeshInfo>(json);
        }

        if (meshInfo == null)
        {
            Debug.LogError("No mesh data");
            return;
        }

        var obj = Selection.activeGameObject;
        meshInfo.BuildObject(obj != null ? obj.transform : null);
    }

    [MenuItem("Tools/Mesh/Save to Data")]
    private static void SaveSelectedToData()
    {
        var obj = Selection.activeGameObject;
        if (obj == null)
        {
            Debug.LogError("No object in selection");
            return;
        }

        var data = CreateAsset<LevelMeshesData>("Level_test_");
        data.SetupData(obj);
    }

    [MenuItem("Tools/Mesh/Load from Data")]
    private static void LoadMeshesFromSelectedData()
    {
        var name = "Level_test_";
        var assetPath = $"{LevelDataPath}{name}{typeof(LevelMeshesData)}.asset";

        var data = AssetDatabase.LoadAssetAtPath(assetPath, typeof(LevelMeshesData)) as LevelMeshesData;
        if (data == null)
        {
            Debug.LogError($"{assetPath}");
            return;
        }

        var obj = Selection.activeGameObject;
        foreach (var info in data.MeshInfos)
        {
            info.BuildObject(obj ? obj.transform : null);
        }
    }

    private static T CreateAsset<T>(string name = "") where T : ScriptableObject
    {
        var asset = CreateInstance<T>();
        var assetPath = AssetDatabase.GenerateUniqueAssetPath(
            $"{LevelDataPath}{name}{typeof(T)}.asset");
        
        AssetDatabase.CreateAsset(asset, assetPath);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
        return asset;
    }
}
