﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MeshBuilder : EditorWindow
{
    [MenuItem("Tools/MeshBuilder/Add noise to mesh")]
    private static void AddNoise()
    {
        var obj = Selection.activeGameObject;
        if (obj == null)
        {
            return;
        }
        var meshFilter = obj.GetComponent<MeshFilter>();

        if (meshFilter == null)
        {
            Debug.LogError("No mesh filter in selection");
            return;
        }

        var vertices = meshFilter.sharedMesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            var pos = vertices[i];
            pos.x += Random.Range(-0.1f, 0.1f);
            pos.y += Random.Range(-0.1f, 0.1f);
            pos.z += Random.Range(-0.1f, 0.1f);
            vertices[i] = pos;
        }

        meshFilter.sharedMesh.vertices = vertices;
    }
    
    [MenuItem("Tools/MeshBuilder/Generate Line")]
    private static void GenerateLine()
    {
        var obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        var meshFilter = obj.GetComponent<MeshFilter>();

        int length = 5;
        
        var vertices = new List<Vector3>();
        var triangles = new List<int>();

        var currentVertex = 0;
        for (int x = 0; x < length; x++)
        {
            for (int y = 0; y < 2; y++)
            {
                vertices.Add(new Vector3(x, y, 0f));

                if (x == 0 || x == length - 1)
                {
                    currentVertex++;
                    continue;
                }

                if (y == 0)
                {
                    triangles.Add(currentVertex - 2);
                    triangles.Add(currentVertex - 1);
                    triangles.Add(currentVertex);
                }

                if (y == 1)
                {
                    triangles.Add(currentVertex - 2);
                    triangles.Add(currentVertex);
                    triangles.Add(currentVertex - 1);
                }
                currentVertex++;
            }
        }

        var mesh = new Mesh
        {
            vertices = vertices.ToArray(),
            triangles = triangles.ToArray()
        };
        
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        meshFilter.sharedMesh = mesh;


    }
}
