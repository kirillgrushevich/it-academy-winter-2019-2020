﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadTracking : MonoBehaviour
{
    public enum Target
    {
        Player,
        Ball,
    }

    [SerializeField] private Target target;
    [SerializeField] private Vector3 fixAngle;
    private GameObject targetObject;

    private void Start()
    {
        if (target == Target.Player)
        {
            targetObject = FindObjectOfType<PlayerController>().gameObject;
        }
    }

    private void LateUpdate()
    {
        transform.rotation = 
            Quaternion.LookRotation(targetObject.transform.position - transform.position);
        
        transform.Rotate(fixAngle);
    }
}
