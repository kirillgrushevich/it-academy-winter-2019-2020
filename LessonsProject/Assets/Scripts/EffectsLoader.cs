﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class EffectsLoader : MonoBehaviour
{
    private AssetBundle bundle;

    private void Start()
    {
        StartCoroutine(LoadBundle());
    }

#if UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            var obj = LoadEffect("Effect1");
            obj.transform.position = Vector3.zero;
            Destroy(obj, 2f);

#if DEBUG
         Debug.Log(1);   
#endif
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            var obj = LoadEffect("Effect2");
            obj.transform.position = Vector3.zero;
            Destroy(obj, 2f);
            
#if DEBUG
            Debug.Log(2);   
#endif
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            var obj = LoadEffect("Effect3");
            obj.transform.position = Vector3.zero;
            Destroy(obj, 2f);
            
#if DEBUG
            Debug.Log(3);   
#endif
        }
    }
#endif

    private IEnumerator LoadBundle()
    {
        var path = "https://drive.google.com/uc?export=download&id=19MZ3UTf6CPPzHKDuU9q49B35RS0mymsf";
        using (var webRequest = UnityWebRequestAssetBundle.GetAssetBundle(path))
        {
            yield return webRequest.SendWebRequest();
            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.LogError(webRequest.error);
            }
            else
            {
                bundle = DownloadHandlerAssetBundle.GetContent(webRequest);
                Debug.Log($"Bundle {bundle.name} loaded");
            }
        }
    }
    
    
    public GameObject LoadEffect(string effectName)
    {

        
        
        if (!bundle)
        {
            var path = Path.Combine(Application.dataPath, "AssetBundles/effects");
            bundle = AssetBundle.LoadFromFile(path);
            Debug.Log("Load from disk");
        }

        if (bundle == null)
        {
            Debug.LogError("Bundle loading error");
            return null;
        }

        var prefab = bundle.LoadAsset<GameObject>(effectName);
        return prefab ? Instantiate(prefab) : null;
        
        //var path = $"Effects/EffectsConfig";
        //var config = Resources.Load<EffectsConfig>(path);
        //var prefab = config.effects[nm];
        //return prefab ? Instantiate(prefab) : null;
    }

}
