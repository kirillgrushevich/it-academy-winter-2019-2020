﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
public class CreateAssetBundles
{
    [MenuItem("Assets/Build Asset bundles")]
    private static void BuildAllAssetBundles()
    {
        BuildPipeline.BuildAssetBundles("Assets/AssetBundles",
            BuildAssetBundleOptions.None, BuildTarget.StandaloneOSX);
        AssetDatabase.Refresh();
    }

    [MenuItem("Assets/Get asset bundles names")]
    private static void GetNames()
    {
        var names = AssetDatabase.GetAllAssetBundleNames();
        foreach (var name in names)
        {
            Debug.Log($"Bundle: {name}");
        }
    }
}
#endif

