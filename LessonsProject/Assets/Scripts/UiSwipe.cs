﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiSwipe : MonoBehaviour
{
    [SerializeField] private SwipeDetector detector;
    [SerializeField] private Transform targetPanel;
    [SerializeField] private AnimationCurve curve;

    private void Start()
    {
        detector.OnSwipe += OnSwipe;
    }

    private void OnSwipe(int direction)
    {
        StartCoroutine(SwipeProcess(direction));
    }

    private IEnumerator SwipeProcess(int direction)
    {
        var startPoint = targetPanel.position;
        var endPoint = startPoint;
        endPoint.x += 5 * direction;

        var k = 0f;
        while (k < 1f)
        {
            var p = curve.Evaluate(k);
            targetPanel.position = Vector3.Lerp(startPoint, endPoint, p);
            k += Time.deltaTime;
            yield return null;
        }
    }
}
