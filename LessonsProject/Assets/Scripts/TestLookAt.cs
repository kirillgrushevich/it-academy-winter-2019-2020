﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLookAt : MonoBehaviour
{
    [SerializeField] private Transform target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var targetRotation = Quaternion.LookRotation(target.position - transform.position);
        
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime);
        
        transform.Translate(transform.forward * Time.deltaTime, Space.World);
    }
}
