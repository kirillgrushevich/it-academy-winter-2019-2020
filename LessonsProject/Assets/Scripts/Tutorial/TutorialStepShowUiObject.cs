using UnityEngine;

namespace Tutorial
{
    public class TutorialStepShowUiObject : TutorialStepUiAbstract
    {
        [SerializeField] private GameObject showObject;
        [SerializeField] private float timer;

        public override bool InitStep()
        {
            showObject.SetActive(false);
            var isAdded = base.InitStep();
            if (isAdded)
            {
                return true;
            }
            
            Destroy(showObject);
            return false;
        }

        public override void StartStep()
        {
            base.StartStep();
            showObject.SetActive(true);
            DelayRun.Execute(StopStep, timer, showObject);
        }

        public override void StopStep()
        {
            base.StopStep();
            Destroy(showObject);
            Destroy(this);
        }
    }
}