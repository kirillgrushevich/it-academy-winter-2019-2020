using UnityEngine;
using UnityEngine.UI;

namespace Tutorial
{
    public class TutorialStepUiButton : TutorialStepUiAbstract
    {
        [SerializeField] private Button button;

        public override bool InitStep()
        {
            button.gameObject.SetActive(false);
            var isAdded = base.InitStep();
            if (isAdded)
            {
                return true;
            }
            
            button.gameObject.SetActive(true);
            return false;
        }

        public override void StartStep()
        {
            base.StartStep();
            button.gameObject.SetActive(true);
            button.onClick.AddListener(StopStep);
        }

        public override void StopStep()
        {
            base.StopStep();
            Destroy(this);
        }
    }
}