﻿namespace Tutorial
{
    public interface ITutorialStep
    {
        int Id { get; }
        bool InitStep();
        void StartStep();
        void StopStep();
    }
}
