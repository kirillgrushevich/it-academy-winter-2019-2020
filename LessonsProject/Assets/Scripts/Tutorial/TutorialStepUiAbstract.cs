using System;
using UnityEngine;

namespace Tutorial
{
    public abstract class TutorialStepUiAbstract : MonoBehaviour, ITutorialStep
    {
        [SerializeField] protected int id;
        public int Id => id;
        
        public virtual bool InitStep()
        {
            return TutorialController.Instance.Add(this);
        }

        public virtual void StartStep()
        {
            
        }

        public virtual void StopStep()
        {
            TutorialController.Instance.Stop(id);
        }

        protected void Awake()
        {
            if (!InitStep())
            {
                Destroy(this);
            }
        }
    }
}