﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAround : MonoBehaviour
{
    [SerializeField] private Transform target;

    private Coroutine aroundCoroutine;
    
    // Update is called once per frame
    private void Update()
    {
        if (aroundCoroutine != null)
        {
            return;
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            aroundCoroutine = StartCoroutine(AroundProcess(2, 15));
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            aroundCoroutine = StartCoroutine(AroundProcess(4, 5));
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            aroundCoroutine = StartCoroutine(AroundProcess(10, 36));
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            aroundCoroutine = StartCoroutine(AroundProcess(5, 180));
        }
        
    }

    private IEnumerator AroundProcess(float time, float speed)
    {
        var timer = 0f;
        while (timer < time)
        {
            transform.RotateAround(target.position, target.up, speed * Time.deltaTime);
            timer += Time.deltaTime;
            yield return null;
        }

        aroundCoroutine = null;
    }
}
