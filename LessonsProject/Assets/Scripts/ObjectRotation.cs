﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    [SerializeField] private Transform targetPoint;

    [SerializeField] private float speed = 45f;


    // Update is called once per frame
    private void Update()
    {
        transform.RotateAround(targetPoint.position, targetPoint.up, speed * Time.deltaTime);
    }
}
