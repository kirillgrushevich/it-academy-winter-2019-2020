﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCar : MonoBehaviour
{
    [SerializeField] private Vector3 centerOfMass = new Vector3(0, -2f, 0);
    [SerializeField] private float maxTorque = 500f;
    [SerializeField] private float maxSteerAngle = 45;
    [SerializeField] private Transform[] wheelMeshes;
    [SerializeField] private WheelCollider[] wheelColliders;

    private Rigidbody rig;
    
    
    // Start is called before the first frame update
    private void Start()
    {
        rig = GetComponent<Rigidbody>();
        rig.centerOfMass = centerOfMass;
    }

    // Update is called once per frame
    private void Update()
    {
        UpdateMeshPositions();
    }

    private void FixedUpdate()
    {
        var steer = Input.GetAxis("Horizontal") * maxSteerAngle;
        var torque = Input.GetAxis("Vertical") * maxTorque;

        wheelColliders[0].steerAngle = steer;
        wheelColliders[1].steerAngle = steer;
        
        for (int i = 0; i < 4; i++)
        {
            wheelColliders[i].motorTorque = torque;
        }
    }

    private void UpdateMeshPositions()
    {
        for (int i = 0; i < 4; i++)
        {
            wheelColliders[i].GetWorldPose(out Vector3 pos, out Quaternion rotation);

            wheelMeshes[i].position = pos;
            wheelMeshes[i].rotation = rotation;
        }
    }
}
