﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestUiString : MonoBehaviour
{
    [SerializeField] private HorizontalLayoutGroup layoutGroup;
    [SerializeField] private Transform root;
    [SerializeField] private Text baseText;

    private List<Text> texts;
    private string text = "Жили у бабуси два веселых гуся. Серый убил белого и отобрал жилплощадь у бабуси!";
    
    // Start is called before the first frame update
    private IEnumerator Start()
    {
        var strings = text.Split(' ');

        layoutGroup.enabled = false;
        
        foreach (var s in strings)
        {
            var obj = Instantiate(baseText, baseText.transform.parent);
            obj.transform.localScale = Vector3.one;

            var textComponent = obj.GetComponent<Text>();
            textComponent.text = s;
        }
        
        baseText.gameObject.SetActive(false);

        yield return null;
        layoutGroup.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        root.Translate(Vector3.left * Time.deltaTime * 200f);
    }
}
